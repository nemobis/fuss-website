---
title: Pro FUSS
subtitle: Noi appoggiamo il progetto FUSS!
---

Un grazie di cuore agli enti, associazioni ed aziende che appoggiano il progetto FUSS e condividono l'obiettivo di avere una scuola digitalmente sostenibile a norma di legge. Se anche tu vuoi sostenere il progetto FUSS, manda una semplice mail a [info@fuss.bz.it](mailto:info@fuss.bz.it) indicando il logo da utilizzare.

## Enti e società pubbliche

|     |     |
| --- | --- |
| <div style="width:180px; display:block; margin:auto">[![Repubblica Digitale](/img/endorsement/repubblica-digitale.png)](https://teamdigitale.governo.it/it/repubblica-digitale)</div> | [Repubblica Digitale](https://teamdigitale.governo.it/it/repubblica-digitale) - Team per la trasformazione digitale (*Il progetto FUSS aderisce a Repubblica Digitale, un’iniziativa del Team per la Trasformazione Digitale della Presidenza del Consiglio dei Ministri*) |
| <div style="width:220px; display:block; margin:auto; padding: 0px 0px 0px 0px">[![Facoltà di Scienze e Tecnologie Informatiche, Libera Università di Bolzano](/img/endorsement/unibz_computer-science.png)](https://www.unibz.it/it/faculties/computer-science/)</div> | [Facoltà di Scienze e Tecnologie Informatiche, Libera Università di Bolzano](https://www.unibz.it/it/faculties/computer-science/) |
| <div style="width:160px; display:block; margin:auto; padding: 20px 0px 20px 0px">[![Infranet SpA](/img/endorsement/infranet.png)](https://www.infranet.bz.it/)</div> | [Infranet SpA](https://www.infranet.bz.it/) |
| <div style="width:160px; padding: 25px 0px 25px 0px; display:block; margin:auto">[![NOI Techpark](/img/endorsement/noi.png)](https://noi.bz.it/)</div> | [NOI Techpark](https://noi.bz.it/) (*Il progetto FUSS collabora nel Knowhow Transfer e specialmente nella formazioni di giovani talenti per la trasformazione digitale nelle aziende*) |

## Associazioni, fondazioni, progetti ed iniziative

|     |     |
| --- | --- |
| <div style="width:150px; padding: 20px 0px 20px 0px; display:block; margin:auto">[![Bergamo Linux User Group](/img/endorsement/bglug.png)](https://bglug.it)</div> | [Bergamo Linux User Group](https://bglug.it) |
| <div style="width:180px; display:block; margin:auto">[![CoderDolomiti - Informatica creativa per ragazzi](/img/endorsement/coderdolomiti.png)](https://www.coderdolomiti.it/)</div> | [CoderDolomiti - Informatica creativa per ragazzi](https://www.coderdolomiti.it/) |
| <div style="width:100px; display:block; margin:auto">[![Digital Sustainability Südtirol-Alto Adige](/img/endorsement/digsus-suedtirol.png)](https://openbz.eu)</div> | [Digital Sustainability Südtirol-Alto Adige](https://openbz.eu) |
| <div style="width:80px; display:block; margin:auto">[![The Document Foundation](/img/endorsement/tdf.png)](https://www.documentfoundation.org/)</div> | FUSS è sostenuto dai membri di [The Document Foundation](https://www.documentfoundation.org/) |
| <div style="width:150px; display:block; margin:auto; padding:10px">[![Free Software Foundation Europe](/img/endorsement/fsfe.png)](https://fsfe.org)</div> | [Free Software Foundation Europe](https://fsfe.org) |
| <div style="width:150px; display:block; margin:auto; padding:10px">[![LibreItalia](/img/endorsement/libreitalia.png)](https://www.libreitalia.org)</div> | [Associazione LibreItalia Onlus](https://www.libreitalia.org) |
| <div style="width:200px; display:block; margin:auto; padding:10px">[![LinuxTrent - Associazione di promozione sociale](/img/endorsement/linuxtrent.png)](https://www.linuxtrent.it)</div> | [LinuxTrent - Associazione di promozione sociale](https://www.linuxtrent.it) |
| <div style="width:100px; display:block; margin:auto">[![LUGBZ - Linux User Group Bozen-Bolzano-Bulsan](/img/endorsement/lugbz.png)](https://www.lugbz.org)</div> | [Linux User Group Bolzano-Bozen-Bulsan](https://www.lugbz.org) |
| <div style="width:120px; display:block; margin:auto">[![The Open Schoolhouse](/img/endorsement/the-open-schoolhouse.png)](https://theopenschoolhouse.com/)</div> | *[The Open Schoolhouse](https://theopenschoolhouse.com/): Building a Technology Program to Transform Learning and Empower Students* by [Charlie Reisinger](https://theopenschoolhouse.com/), K12 EdTech Leader - Open Source Advocate |
| <div style="width:120px; display:block; margin:auto">[![OSI - Open Source Initiative](/img/endorsement/osi.png)](https://opensource.org/affiliates)</div> | The FUSS Project is [Affiliate Member of the Open Source Initiative](https://opensource.org/affiliates) |
| <div style="width:150px; display:block; margin:auto; padding: 20px 0px 20px 0px">[![hds unione](/img/endorsement/hds-unione.jpg)](https://www.hds-bz.it)</div> | [hds unione - handels- <i>und</i> dienstleistungsverband <i>Südtirol</i> - unione commercio turismo servizi <i>Alto Adige</i>](https://www.hds-bz.it) |
| <div style="width:180px; padding: 20px 0px 20px 0px; display:block; margin:auto">[![Teatro Cristallo](/img/endorsement/cristallo.png)](https://www.teatrocristallo.it/)</div> | [Teatro Cristallo Associazione - Verein](https://www.teatrocristallo.it/) |
| <div style="width:200px; display:block; margin:auto">[![Verbraucherzentrale Südtirol - Centro Tutela Consumatori Utenti](/img/endorsement/verbraucherzentrale.png)](https://www.consumer.bz.it)</div> | [Verbraucherzentrale Südtirol - Centro Tutela Consumatori Utenti](https://www.consumer.bz.it) |


## Aziende e liberi professionisti

|     |     |
| --- | --- |
| <div style="width:150px; display:block; margin:auto">[![Christian Anton Mair - Sviluppo Software e Formazione](/img/endorsement/1006org.png)](https://www.1006.org)</div> | [Christian Anton Mair - Sviluppo Software e Formazione](https://www.1006.org) |
| <div style="width:150px; padding: 20px 0px 20px 0px; display:block; margin:auto">[![BgWorld S.r.l.](/img/endorsement/bgworld.png)](http://www.bgworld.it)</div> | [BgWorld S.r.l.](http://www.bgworld.it) |
| <div style="width:190px; display:block; margin:auto">[![Andrea Congiu - Infermiere Libero Professionista](/img/endorsement/congiu-andrea.png)](https://www.andrea-congiu.it)</div> | [Andrea Congiu - Infermiere Libero Professionista](https://www.andrea-congiu.it) |
| <div style="width:150px; display:block; margin:auto">[![Cooperativa Call](/img/endorsement/coop-call.png)](https://www.coopcall.it)</div> | [Cooperativa Call](https://www.coopcall.it) |
| <div style="width:180px; display:block; margin:auto">[![Endian](/img/endorsement/endian.png)](https://www.endian.com) | [Endian Srl](https://www.endian.com)</div> |
| <div style="width:120px; display:block; margin:auto">[![endo7 srl - IT Agency](/img/endorsement/endo7.png)](https://www.endo7.com) | [endo7 srl - IT Agency](https://www.endo7.com)</div> |
| <div style="width:200px; padding: 20px 0px 20px 0px; display:block; margin:auto">[![Ethical Software](/img/endorsement/ethicalsoftware.png)](http://www.ethicalsoftware.it/)</div> | [Ethical Software Coop. Soc. ONLUS](http://www.ethicalsoftware.it/) |
| <div style="width:100px; display:block; margin:auto; padding:10px">[![ForTeam - Knowledge Company](/img/endorsement/forteam.png)](https://www.forteam.it)</div> | [ForTeam - Knowledge Company](https://www.forteam.it/) |
| <div style="width:150px; display:block; margin:auto; padding: 10px">[![HydroloGIS Environmental Engineering](/img/endorsement/hydrologis.png)](https://www.hydrologis.com/)</div> | [HydroloGIS Environmental Engineering](https://www.hydrologis.com/) |
| <div style="width:150px; display:block; margin:auto; padding:20px 0px 20px 0px">[![Limitis Internet Service Provider](/img/endorsement/limitis.png)](https://www.limitis.com)</div> | [Limitis Internet Service Provider](https://www.limitis.com) |
| <div style="width:180px; padding: 25px 0px 25px 0px; display:block; margin:auto">[![Marco Marinello](/img/endorsement/marinello-marco.png)](https://marcomarinello.it)</div> |  [Marco Marinello - Developer & SysAdmin](https://marcomarinello.it) |
| <div style="width:150px; padding: 25px 0px 25px 0px; display:block; margin:auto">[![Davide Montesin - Innovative Software](/img/endorsement/montesin-davide.png)](http://www.davide.bz)</div> | [Davide Montesin - Innovative Software](http://www.davide.bz) |
| <div style="width:150px; display:block; margin:auto">[![Omnis Systems](/img/endorsement/omnissystems.png)](https://www.omnis-systems.com)</div> | [Omnis Systems Ltd](https://www.omnis-systems.com) |
| <div style="width:180px; display:block; padding: 20px 0px 20px 0px; margin:auto">[![QBus Srl - Information & Technology Group](/img/endorsement/qbus.png)](https://www.qbus.it)</div> | [QBus Srl - Information & Technology Group](https://www.qbus.it) |
| <div style="width:200px; padding: 25px 0px 25px 0px; display:block; margin:auto">[![Studio Creating](/img/endorsement/studio-creating.png)](https://studiocreating.it/)</div> | [Studio Creating](https://studiocreating.it/) |
| <div style="width:200px; display:block; margin:auto; padding: 25px 0px 25px 0px">[![Teamblau](/img/endorsement/teamblau.gif)](https://www.teamblau.com)</div> | [teamblau GmbH - internetmanufaktur](https://www.teamblau.com) |
| <div style="width:200px; display:block; margin:auto">[![Truelite](/img/endorsement/truelite.png)](https://www.truelite.it/)</div> | [Truelite Srl](https://www.truelite.it/) |
