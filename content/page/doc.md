---
title: Documentazione
#subtitle: 
---

Qui potete trovare le guide necessarie per installare, usare, manutenere e contribuire allo sviluppo della  distribuzione FUSS.

- [Manuale utente](https://fuss-user-guide.readthedocs.io)
- [Manuale per il tecnico](https://fuss-tech-guide.readthedocs.io) 
  - [Guida rapida all'installazione](https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html) 
- [Manuale per lo sviluppatore](https://fuss-dev-guide.readthedocs.io/it/latest/)

La gestione dei singoli progetti software e di documentazione viene fatta con un'istanza di Redmine raggiungibile al link

- [Progetti software](https://work.fuss.bz.it/projects)
