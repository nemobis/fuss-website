---
title: Contatti
#subtitle: 
---

Per qualsiasi domanda su FUSS o se desideri collaborare, scrivi una mail a <info@fuss.bz.it>.

Iscriviti alle nostre mailing list:

- [fuss-community](https://www.fuss.bz.it/cgi-bin/mailman/listinfo/fuss-community): Discussioni generali riguardo a FUSS
- [fuss-devel](https://www.fuss.bz.it/cgi-bin/mailman/listinfo/fuss-devel): Coordinamento attività di sviluppo software
