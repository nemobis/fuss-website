---
title: Download...
subtitle: Scarica server e client FUSS e crea una rete didattica
---

## Server

- Per installare un FUSS Server (sia esso fisico o virtuale), si usi semplicemente l'immagine di Debian Netinst 8, come indicato nella [FUSS Tech Guide](https://fuss-tech-guide.readthedocs.io/it/latest/installazioni_specializzate/installazione-fuss-server-debian.html).

## Client

La ISO client può essere copiata su chiavetta USB o DVD ed utilizzata in tre diverse modalità: 1) live, senza la necessità di installarla; 2) desktop standalone, installandola su di un PC satellite non legato ad una specifica rete didattica 3) come client connesso ad un server di una rete didattica.

- [FUSS Client ISO v. 9.5.1 64bit](http://iso.fuss.bz.it/fuss9/client/fuss-client-9.5.1-amd64-xfce.iso) (5.0 GB)
- [FUSS Client ISO v. 9.5.1 32bit](http://iso.fuss.bz.it/fuss9/client/fuss-client-9.5.1-i386-xfce.iso) (5.1 GB)
 
Sono disponibili anche [*immagini non ufficiali*](http://iso.fuss.bz.it/fuss9/client/unofficial-images/) del client (per le architetture i386, amd64, armhf) installabili mediante [Clonezilla](https://clonezilla.org/).
