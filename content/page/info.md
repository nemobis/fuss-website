---
title: Info
subtitle: Breve storia di FUSS e materiale informativo
---

Il **progetto FUSS** (Free Upgrade in South Tyrol's Schools) è partito nel 2005 con lo scopo di portare [Software Libero](https://it.wikipedia.org/wiki/Software_libero) nelle scuole della Provincia Autonoma di Bolzano. Il progetto è stato finanziato dal Fondo Sociale Europeo e gestito amministrativamente dalla [Scuola Professionale Einaudi di Bolzano](http://www.cts-einaudi.fpbz.it) la quale ha scelto di avvalersi della consulenza e collaborazione di una azienda italiana operante nel mercato informatico del Software Libero, la ditta [Truelite Srl](https://www.truelite.it/). Dal 2006 al 2015 il progetto è proseguito con personale docente del Dipartimento Formazione e Istruzione italiana adeguatamente formato per supportare i docenti nella didattica e gestire il parco macchine (circa 4000 desktop e 65 server) presenti nelle scuole.
Dal 2015 la Ripartizione informatica della Provincia di Bolzano ha preso in consegna l'assistenza tecnica con un gruppo di sistemisti GNU/Linux (**Tecnici FUSS**). 

Il governo del progetto è in capo alla [Direzione Istruzione e Formazione Italiana](http://www.forumpachallenge.it/proponenti/direzione-istruzione-e-formazione-italiana-provincia-autonoma-di-bolzano) della Provincia Autonoma di Bolzano con il **Nucleo FUSS**, formato da un coordinatore (Paolo Dongilli) coadiuvato dagli insegnanti Andrea Bonani, Claudio Cavalli e Stefania Fiore, dallo studente Marco Marinello e con la collaborazione dell'ispettore per l'ambito matematico, scientifico e tecnologico Mauro Valer.

Presso ciascun istituto scolastico è presente la figura del **referente tecnico**; in totale sono circa 70 docenti i quali fungono da punto di contatto verso i tecnici FUSS, per richieste di assistenza e manutenzione e verso il Nucleo FUSS, per richieste relative al software installato, richieste di nuovo software o richieste di formazione.

Nucleo FUSS, Tecnici e Referenti compongono insieme il **Gruppo FUSS** che ha come scopo quello di sviluppare, manutenere e divulgare un progetto all’avanguardia sul territorio nazionale che ha permesso di rendere digitalmente sostenibile la didattica con le TIC (tecnologie dell’informazione e della comunicazione) nelle scuole in lingua italiana grazie a quattro obiettivi fondamentali: 

* l’utilizzo di Software Libero, 
* l’impiego di formati aperti e 
* la creazione di contenuti liberi 

ponendo così le basi per il quarto obiettivo il cui raggiungimento dovrebbe essere garantito da ogni scuola per definizione: il **libero accesso al sapere**.

## Il modello didattico

La scelta di usare software libero nella scuola è anzitutto, al di là delle ragioni economiche o tecniche, una scelta etica e politica. È cioè la scelta di rifarsi, nell'insegnamento, ai valori della libertà e della condivisione del sapere, e non solo quella di usare software efficiente, stabile e sicuro.

La filosofia che sta alla base del software libero, quella della libertà di accesso alle informazioni e della condivisione della conoscenza, si adatta naturalmente al compito educativo di una nuova scuola.

Inoltre usare software libero rappresenta la scelta di utilizzare patrimonio comune dell'umanità, il cui miglioramento e diffusione vanno a beneficio di tutti, e non di una singola entità.

L'utilizzo del software libero rende possibile la partecipazione diretta al suo sviluppo da parte di studenti ed insegnanti, non solo come scrittura di codice, ma soprattutto in termini di suggerimenti sul funzionamento, produzione di documentazione, traduzioni, realizzazioni di contenuti, ecc. 
Tutto ciò in un modello di scuola vista come una comunità in cui tutte le sue componenti partecipano attivamente al processo di costruzione della conoscenza.

Obiettivo a medio-lungo termine di questo progetto è favorire metodologie di didattica collaborativa, attraverso il coinvolgimento attivo sia dei docenti che degli studenti nello sviluppo del progetto stesso.

## Materiale informativo

E' disponibile una brochure sul progetto in due varianti bilingui:

- [Brochure FUSS tedesco-italiano](/material/brochure-fuss-de-it-v4.pdf)
- [Brochure FUSS inglese-italiano](/material/brochure-fuss-en-it-v4.pdf)

![foto brochure](/img/foto-volantino.jpg)

