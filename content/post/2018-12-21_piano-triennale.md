---
title: "Piano per lo sviluppo delle tecnologie della società dell'informazione nella didattica"
subtitle: "La Direzione Istruzione e Formazione italiana della Provincia Autonoma di Bolzano ha pubblicato il nuovo Piano e le linee guida correlate"
date: 2018-12-21T23:00:12+02:00
draft: false
---

Ogni tre anni la **Direzione Istruzione e Formazione italiana** della **Provincia Autonoma di Bolzano predispone**, per le scuole di ogni ordine e grado, il [**Piano per lo sviluppo delle tecnologie della società dell'informazione nella didattica e le linee guida ad esso correlate (download PDF)**](http://www.provinz.bz.it/formazione-lingue/scuola-italiana/downloads/Piano_e_linee_guida_2019-2021.pdf). La pubblicazione del Piano 2019-2021 prosegue e rilegge, alla luce dell'evoluzione in corso, le azioni promosse con i precedenti documenti programmatici per l’uso didattico delle Tecnologie della Società dell'Informazione (TSI).

Il piano è concepito in sinergia tra le diverse istituzioni della direzione Istruzione e Formazione, al fine di indirizzare le azioni delle Scuole autonome, le quali, nel redigere il loro piano di utilizzo delle TSI accorderanno queste linee-obiettivo con la specifica realtà nella quale operano e con le particolarità della loro offerta formativa.

<!--more-->

I temi trattati, ovvero gli obiettivi di sistema, riguardano sia l’aspetto strumentale delle TSI, sia quello legato alla loro valenza didattica e formativa. Anche nel prossimo triennio si intende valorizzare il più possibile la progettazione condivisa nei Consigli di classe con l’intento di rendere le tecnologie un effettivo strumento di azione formativa, nonché di valutazione condivisa, come del resto previsto dalle Indicazioni provinciali. Il piano mette, inoltre, in risalto la progressività dell’inserimento e l’uso delle TSI nella pratica didattica, affinché questo sia graduale e mirato: nella Scuola primaria, per salvaguardare l'intero e ricco patrimonio di attività manuali e pratiche; nella Scuola secondaria di I e II grado per favorire un avvicinamento progressivamente sempre più autonomo e consapevole alle tecnologie.

La Direzione Istruzione e Formazione italiana quattordici anni fa ha optato per un percorso di [**sostenibilità digitale**](https://openbz.eu/) nella didattica con il progetto [FUSS](https://fuss.bz.it)  [(https://fuss.bz.it)](https://fuss.bz.it) che si fonda su quattro obiettivi per le scuole di ogni ordine e grado in lingua italiana della Provincia Autonoma di Bolzano:

* **utilizzo di software libero**;
* **impiego di standard aperti**;
* **creazione di contenuti liberi**;
* **libero accesso al sapere**.

Per il raggiungimento di questi obiettivi, il piano prevede il rafforzamento delle competenze del personale docente, attraverso una pluralità di iniziative:

* azioni specifiche, come previste dal piano annuale di formazione per dirigenti e docenti con l'obiettivo che questi diventino, nelle rispettive scuole,  “divulgatori” delle competenze acquisite;
* introduzione ed uso di strumenti [FLOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) per l’inclusione, messi a disposizione dall’Istituto per le Tecnologie Didattiche di Genova (CNR) nell’ambito del progetto [So.Di.Linux](https://sodilinux.itd.cnr.it/);
* sessioni di formazione a richiesta, condotte dal Nucleo FUSS, per referenti tecnici e docenti, da tenersi presso le singole scuole;
* sviluppo di manualistica per utenti, referenti e tecnici, riguardanti il software installato nelle scuole;
* sviluppo di un progetto annuale per l’accoglienza, in convenzione con le scuole di appartenenza, di studenti in alternanza scuola lavoro;
* disponibilità di uno [sportello di consulenza gratuita](https://fuss.bz.it/open-linux-desk), promosso dall’Intendenza scolastica in lingua italiana, attivo già dal 2017 nelle città di Bolzano, Merano e Brunico;
* a sostegno della didattica e per promuovere le competenze di cittadinanza ritenute essenziali, verranno sviluppate ulteriormente le piattaforme di e-learning e documentale.

L’utilizzo di software libero e di standard aperti nelle scuole rappresenta, inoltre, un volano in grado di agevolare i processi innovativi, che partono dalle scuole stesse, con  progetti didattici specifici nell’ambito dell’informatica puntando a collaborazioni con NOI-Techpark di Bolzano, Libera Università di Bolzano e con atenei ed altre istituzioni nazionali ed internazionali.

L'amministrazione, sia centralmente, sia nelle segreterie scolastiche, ha l’opportunità di continuare ad appoggiare la proposta di sostenibilità digitale utilizzando Software Libero, ed in particolare la suite [LibreOffice](https://it.libreoffice.org/) già installata in ogni postazione e periodicamente aggiornata. L’utilizzo di tale software, inoltre, migliorerà la compatibilità tra i documenti creati utilizzando le postazioni didattiche della scuola e quelli prodotti dall'amministrazione.

**Link al Piano triennale (PDF):** [http://www.provinz.bz.it/formazione-lingue/scuola-italiana/downloads/Piano_e_linee_guida_2019-2021.pdf](http://www.provinz.bz.it/formazione-lingue/scuola-italiana/downloads/Piano_e_linee_guida_2019-2021.pdf)
