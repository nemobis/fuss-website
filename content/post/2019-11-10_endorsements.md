---
title: "Per una scuola digitalmente sostenibilie"
subtitle: "Enti, associazioni, aziende e cittadini offrono il loro supporto a FUSS"
date: 2019-11-10T21:42:11+02:00
draft: false
---

[![FUSS supporters - 10.11.2019](/img/endorsement/supporters-20191110.png)](https://fuss.bz.it/page/profuss)

Un grazie di cuore agli [**enti, associazioni, aziende e cittadine/i che appoggiano il progetto FUSS**](https://fuss.bz.it/page/profuss) e condividono l’obiettivo di avere una scuola digitalmente sostenibile a norma di legge. Se anche tu vuoi sostenere il progetto FUSS, manda una semplice mail a [**info@fuss.bz.it**](mailto:info@fuss.bz.it) indicando il logo da utilizzare.
<!--more-->
