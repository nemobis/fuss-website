---
title: "FUSS is a proud OSI member"
subtitle: "The Open Source Initiative board admits the FUSS Project as one of its affiliate members"
date: 2019-09-20T23:14:16+02:00
draft: false
---

[![OSI Logo](/img/osi.png)](https://opensource.org/affiliates)

Another achievement for the FUSS Project that was acceptet on Friday 20th 2019 as [**Affiliate Member of the Open Source Initiative**](https://opensource.org/affiliates). 
<!--more-->
Affiliate membership is an ideal way for open source projects, and the communities that support them, to promote and extend the [OSI mission](https://opensource.org/about), and contribute to the continued awareness and adoption of Open Source Software.
