---
title: "FUSS all'IPSEOA \"E. Cornaro\" di Jesolo:"
subtitle: "10 anni di sostenibilità digitale raccontati da Luca Marcolongo"
date: 2018-07-12T17:29:13+02:00
draft: false
---

Luca Marcolongo, responsabile informatico presso l'Istituto
Professionale "E. Cornaro" per i Servizi di Enogastronomia e
Ospitalità Alberghiera di Jesolo, ci racconta l'esperienza maturata
negli ultimi 10 anni con FUSS.

<!--more-->

> ![IPSOEA Cornaro](/img/ipseoa-scuola.jpg)

> Il nostro Istituto ha adottato il sistema FUSS nel 2008 soprattutto
per la parte server. All’epoca dovevamo trovare un’alternativa al
nostro primo sistema operativo di rete che era Novell Netware e da
tempo guardavamo con molto interesse a Linux. Tenendo conto che
avevamo solo client con s.o. Windows, cercavamo un sistema per la
gestione del dominio (utenti e condivisioni) che fosse libero e
pertanto riusabile.

> Abbiamo testato FUSS Server e dopo aver fatto un po’ di formazione con
la ditta [Truelite](https://www.truelite.it), lo abbiamo adottato e ne
siamo rimasti molto soddisfatti.

> Nel 2013, con l’introduzione del registro elettronico, abbiamo
acquistato per le nostre aule didattiche dei notebook (processore i3 e
4 GB RAM) senza sistema operativo e su di esse abbiamo installato
Linux (Ubuntu), FUSS client e il software necessario per la didattica
e per l’utilizzo delle LIM. Con i soldi che abbiamo risparmiato
evitando l'acquisto di software proprietario, abbiamo disegnato e
fatto realizzare delle scrivanie didattiche con coperchio e serratura
da posizionare in ogni aula.

> ![Aula presso IPSOEA Cornaro](/img/ipseoa-classe.jpg)

> Attualmente al nostro server FUSS di dominio sono collegati circa
150 personal computer Windows (7 e 10). Tutti gli studenti, docenti e
personale ATA hanno il proprio account di rete (circa 900 studenti,
120 docenti/personale ATA) e i 40 notebook collegati alle LIM delle
aule didattiche con Ubuntu e FUSS client. C’è stato molto lavoro da
fare ma la soddisfazione di dare ai nostri studenti l’opportunità di
lavorare con software libero, ci ha interamente ripagato.

> Per quanto riguarda i docenti dell'Istituto, con il tempo hanno preso
confidenza con Linux e diversi ne hanno richiesto l'installazione sul
proprio personal computer o notebook.

> Appena possibile intendiamo programmare il passaggio a FUSS 9 (Debian
9 "stretch") ed aggiornare i PC presenti nelle aule con SSD da 120 GB
in modo da poterli utilizzare ancora per qualche anno.

> Concludo ringraziando tutti coloro che in questi anni hanno lavorato
e contribuito al Progetto FUSS e all'introduzione di Software Libero
nella didattica; auguro loro e a tutte le scuole di continuare su
questa strada di condivisione e riuso delle risorse pubbliche per un
processo di digitalizzazione che sia sempre più sostenibile.