---
title: "FUSS aggiornato anche al Toniolo"
subtitle: "Rinnovato l'impegno del Liceo 'G. Toniolo' di Bolzano per una digitalizzazione sostenibile della didattica con Software Libero"
date: 2018-12-13T23:32:25+02:00
draft: false
---

Nel corso del mese di dicembre il [**Liceo paritario delle scienze applicate allo sport "G. Toniolo"**](https://liceotoniolo.bz.it/) di Bolzano ha rinnovato l'infrastruttura (server e PC) della propria aula informatica. Grazie al costante impegno del dirigente Esio Zaghet a favore di una didattica aperta mediante l'uso di Software Libero, il [**Nucleo FUSS**](https://fuss.bz.it/page/contact) dell'Intendenza scolastica italiana si è reso disponibile a seguire l'installazione di server e PC con l'ultima versione di [FUSS](https://fuss.bz.it) ed a formare i referenti tecnici del liceo, come offerto anche agli altri istituti paritari della provincia che intendano adottare Software Libero ed uscire da situazioni di [*vendor lock-in*](https://en.wikipedia.org/wiki/Vendor_lock-in) derivanti dall'uso software proprietario.

![Aula informatica Liceo Toniolo](/img/toniolo_2018-12.jpg)

<!--more-->

Il Liceo Toniolo in tal modo permette ai propri studenti di continuare come negli anni scorsi ad usare a casa lo stesso software utilizzato in aula senza alcun aggravio di costo per le famiglie.

Anche il [Liceo scientifico delle scienze applicate presso l'istituto paritario "Rainerum"](http://www.rainerum.it/liceo-scientifico-delle-scienze-applicate) di Bolzano, grazie all'impegno dello studente [Marco Marinello](http://www.provincia.bz.it/news/it/news.asp?news_action=4&news_article_id=543511), sta adottando FUSS nel laboratorio di robotica educativa. 

Per i docenti, gli studenti ed i genitori che intendano farsi installare FUSS gratuitamente sui propri PC o notebook, è disponibile lo [Sportello Open & Linux](https://www.fuss.bz.it/open-linux-desk/) a [Bolzano](https://www.fuss.bz.it/open-linux-desk/pages/10-about.html), [Merano](https://www.fuss.bz.it/open-linux-desk/pages/12-about-meran.html) e [Brunico](https://www.fuss.bz.it/open-linux-desk/pages/11-about-bruneck.html), due volte al mese.
