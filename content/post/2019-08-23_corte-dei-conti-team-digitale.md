---
title: "Ridurre la spesa pubblica"
subtitle: "La Corte dei conti verificherà nei suoi audit il riuso del software nel settore pubblico"
date: 2019-08-23T22:33:12+02:00
draft: false
---

La Corte dei conti, che controlla le finanze pubbliche, includerà nei suoi audit i risparmi generati da tutti i servizi pubblici del paese che condividono il software creato da o per loro conto. Il motivo è che **i servizi pubblici che non condividono soluzioni software impediscono ad altri di riutilizzarli, aumentando così i costi per la società**. Questo è uno dei numerosi controlli introdotti di recente che si concentrano sulla digitalizzazione governativa.

<!--more-->

L'attenzione alla condivisione e al riutilizzo di soluzioni software è il risultato di un accordo con il [Commissario per l'attuazione dell'agenda digitale](https://teamdigitale.governo.it/it/people/luca-attias.htm). In dicembre il commissario ha chiesto alla corte di verificare se i servizi pubblici utilizzano le tecnologie digitali per ridurre i costi, compreso l'uso di servizi di cloud computing, eID e pagamenti elettronici.

Il [Codice dell'amministrazione digitale](https://docs.italia.it/italia/piano-triennale-ict/codice-amministrazione-digitale-docs/it/v2017-12-13/) - e in particolare l'[articolo 69](https://docs.italia.it/italia/piano-triennale-ict/codice-amministrazione-digitale-docs/it/v2017-12-13/_rst/capo6_art69.html) - impone alle pubbliche amministrazioni di condividere le applicazioni software create da o per loro.

L'articolo 69 del Codice, tradotto anche in inglese dal team di modernizzazione del governo italiano (Team Digitale), recita:

> "Le pubbliche amministrazioni che siano titolari di soluzioni e programmi informatici realizzati su specifiche indicazioni del committente pubblico, hanno l’obbligo di rendere disponibile il relativo codice sorgente, completo della documentazione e rilasciato in repertorio pubblico sotto licenza aperta, in uso gratuito ad altre pubbliche amministrazioni o ai soggetti giuridici che intendano adattarli alle proprie esigenze, salvo motivate ragioni di ordine e sicurezza pubblica, difesa nazionale e consultazioni elettorali."

"La corte sta ora analizzando se i servizi pubblici stanno diventando adeguatamente digitali", spiega [Alessandro Ranellucci](https://twitter.com/alranel), responsabile open source di [Developers Italia](https://developers.italia.it/), il team di sviluppo software e parte del Team Digitale. "I servizi pubblici che adottano il framework italiano di condivisione e riutilizzo del software possono raggiungere efficienze significative. Oppure, al contrario, la mancanza di una digitalizzazione efficace crea danni fiscali".

Secondo Ranellucci, il nuovo ciclo di audit sta per iniziare. I risultati potrebbero essere attesi all'inizio del prossimo anno.

## Maggiori informazioni: ##

- [Linee Guida su acquisizione e riuso di software per le pubbliche amministrazioni](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/index.html)
- [Developers Italia, Il punto di riferimento per il software della Pubblica Amministrazione](https://developers.italia.it/)

*(Articolo originale in lingua inglese: [To lower cost for society - Italy’s top financial inspectors to include public sector software reuse in their audits a cura di Gijs Hillenius](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/lower-cost-society). Tradotto con il consenso di OSOR)*