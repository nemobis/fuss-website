---
title: "A scuola con l'open source, anche da remoto"
subtitle: "Il nostro impegno in questo momento di difficoltà"
date: 2020-03-19T21:06:31+01:00
draft: true
---

![Studente che utilizza il software FUSS Campus](/img/campus_used.jpg)

Image credits: Raffaele Iovino (CC-BY-SA)


In questo momento di difficoltà per tutta Italia,
FUSS non smette di essere al fianco dei docenti e
degli studenti della Provincia.

<!-- more -->

Dall'inizio di questa emergenza abbiamo lavorato
senza sosta per scalare la nostra infrastruttura
ed adeguarci al carico comprensibilmente
crescente registrato sui nostri sistemi.

Chamilo, nella sua istanza [FUSS Campus](https://campus2.fuss.bz.it),
è la piattaforma online che al momento viene utilizzata di
più, complice probabilmente l'estrema immediatezza.
Studenti delle scuole di ogni ordine e grado hanno modo
di continuare a studiare ed intergagire con i propri docenti
grazie a questo strumento.

Allo stesso tempo, il [Linux User Group di Bolzano](https://lugbz.org) ha
tempestivamente provveduto ad approntare un proprio servizio di
videoconferenza basato su [BigBlueButton](https://bigbluebutton.org)
fornito gratuitamente alle scuole garantendo, come richiede il Ministero,
la conformità alle norme sulla protezione dei dati personali,
la sovranità digitale e che i dati degli utenti non vengano utilizzati
per scopi commerciali.

Anche il gruppo di [Sostenibilità digitale](https://openbz.eu) è un
attore importante: grazie alla collaborazione di molti volontari
e alle donazioni di generose aziende sta fornendo alle famiglie
che non ne sono dotate un PC con FUSS con cui gli studenti
possano seguire le lezioni.

Rimangono a disposizione, per gli utenti già registrati,
anche NextCloud, GroupOffice e le altre soluzioni normalmente
offerte nell'ambito del Progetto. Per le scuole superiori
si consiglia l'utilizzo di [Moodle](https://moodle.fuss.bz.it)
al posto di FUSS Campus.


Con affetto e sperando di rivedervi presto a scuola,

Il Nucleo FUSS
