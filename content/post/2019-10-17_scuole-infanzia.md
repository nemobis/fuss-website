---
title: "FUSS va all'asilo!"
subtitle: "E` stato scelto anche dalle scuole dell'infanzia per rimpiazzare sistemi obsoleti"
date: 2019-10-17T13:27:15+02:00
draft: false
---

![Foto installazione FUSS in una scuola dell'infanzia](/img/scuola-infanzia-rds.png)

Le attrezzature informatiche nelle scuole dell'infanzia sono spesso datate e montano versioni insicure di sistemi operativi proprietari rilasciati anche più di 10 anni fa.
Diverse le richieste giunte al Nucleo FUSS nelle quali le direzioni richiedono sistemi aggiornati e fruibili dalle maestre.
<!--more-->
Si è iniziato con l'Istituto Pluricomprensivo Europa 1 ed in particolare le Scuole dell'infanzia "La Fiaba" e "Raggio di Sole" alle quali è seguita la Scuola dell'infanzia di S. Michele Appiano. La versione di FUSS installata incorpora tutto il software educativo utilizzato dalle scuole in lingua italiana della provincia e si contano tra questi utili applicativi adatti all'età prescolare.
