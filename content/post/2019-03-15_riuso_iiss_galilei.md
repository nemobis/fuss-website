---
title: "Riuso e sostenibilità ambientale"
subtitle: "L'IISS 'G. Galilei' ridà vita a 30 PC"
date: 2019-03-15T23:32:25+02:00
draft: false
---

Catalin Bottazzi e Davide Trevisan, due studenti dell'[**IISS "G. Galilei" di Bolzano**](http://www.iisgalilei.eu/), seguiti dal loro tutor Fabio Ferraris, ridanno vita a 30 PC HP Compaq dc7800. Durante il loro periodo di alternanza scuola-lavoro, dopo aver verificato il corretto funzionamento dei PC in dismissione da parte del loro istituto, Catalin e Daniel hanno rimosso i 30 dischi fissi preesistenti sostituendoli con dischi a stato solido (SSD) forniti dall'Intendenza scolastica italiana.

![Fabio Ferraris con gli studenti Catalin Bottazzi e Trevisan Davide](/img/iiss-galilei-riuso-2019-03_01.jpg)

<!--more-->

Le foto che seguono mostrano la fase di rimozione dei vecchi HDD (che pure verranno riutilizzati) ed il montaggio di dischi SSD (solid state drive) da 120 GB.

![Rimozione dell'hard disk preesistente](/img/iiss-galilei-riuso-2019-03_02.jpg)

![Particolare dei connettori di alimentazione e dati di un disco [SATA](https://it.wikipedia.org/wiki/Serial_ATA)](/img/iiss-galilei-riuso-2019-03_03.jpg)

![Disco SSD al termine del montaggio](/img/iiss-galilei-riuso-2019-03_04.jpg)

A seguire su ciascun PC è stata installata l'immagine di [FUSS 9](https://fuss.bz.it) mediante [Clonezilla](https://clonezilla.org/), il programma di clonazione dischi.

Infine i PC installati, completi di monitor, tastiera e mouse sono stati consegnati dai due studenti, accompagnati dal proprio tutor, in diverse scuole del territorio che necessitavano di sostituzioni urgenti o di PC addizionali per attività didattiche.

Un ringraziamento all'IISS "G. Galilei" per il proprio contributo all'economia circolare mediante il riuso di attrezzature informatiche.
