---
title: "12 anni di sostenibilità digitale nella scuola con FUSS"
subtitle: "Presentazione alla conferenza di servizio dei dirigenti scolastici"
date: 2018-01-11T18:40:53+02:00
draft: false
---

[![presentation screenshot](/material/2018-01-11_FUSS_Conferenza-di-servizio.png)](/material/2018-01-11_FUSS_Conferenza-di-servizio.pdf)

