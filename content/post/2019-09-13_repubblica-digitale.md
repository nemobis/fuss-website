---
title: "Repubblica Digitale: una, accessibile e consapevole"
subtitle: "Il progetto FUSS accolto dal Team per la Trasformazione Digitale della Presidenza del Consiglio dei Ministri"
date: 2019-09-13T19:13:17+02:00
draft: false
---

![Logo Repubblica Digitale](/img/endorsement/repubblica-digitale.png) Un'altra data da non dimenticare per il progetto FUSS: il **13 settembre 2019** il più grande progetto nazionale di sostenibilità digitale nella scuola riceve la notizia che l'adesione all'iniziativa [**Repubblica Digitale**](https://teamdigitale.governo.it/it/repubblica-digitale) del Team per la Trasformazione Digitale della Presidenza del Consiglio dei Ministri è stata ufficialmente accettata.
<!--more-->
Il progetto FUSS è pertanto uno dei progetti che contribuisce e contribuirà alla costruzione del "sistema operativo" del Paese.
Ulteriori informazioni sul sito dell'iniziativa nazionale: **https://teamdigitale.governo.it/it/repubblica-digitale**.
