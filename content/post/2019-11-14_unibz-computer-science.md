---
title: "FUSS accolto all'UNIBZ"
subtitle: "La Facoltà di Scienze e Tecnologie Informatiche sostiene il progetto FUSS"
date: 2019-11-14T22:15:19+02:00
draft: false
---

[![Logo Facoltà di Scienze e Tecnologie Informatiche, UNIBZ](/img/endorsement/unibz_computer-science.png)](https://fuss.bz.it/page/profuss)

Un ringraziamento particolare al Consiglio della **Facoltà di Scienze e Tecnologie Informatiche** della **Libera Università di Bolzano** che ha approvato con **delibera n. 114 del 14.11.2019** di supportare il progetto FUSS.
<!--more-->
