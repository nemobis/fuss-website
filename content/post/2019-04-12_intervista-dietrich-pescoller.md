---
title: "Software Libero per comprendere i misteri dell'universo"
subtitle: "FUSS-Lab intervista l'Ing. Dietrich Pescoller"
date: 2019-04-12T17:39:23+02:00
draft: false
---

[Dietrich Pescoller](https://www.sfscon.it/speakers/dietrich-pescoller/) è un ingegnere informatico/automazione che lavora da quasi 20 anni presso la [Microgate s.r.l.](http://www.microgate.it/) di Bolzano, azienda leader mondiale nella produzione di sofisticati sistemi di controllo per grandi telescopi. In particolare la Microgate ha realizzato il sistema di controllo e metrologia del [Radiotelescopio ALMA](https://it.wikipedia.org/wiki/Atacama_Large_Millimeter_Array) che è stato l'elemento più sensibile all'interno del [progetto EHT (Event Horizon Telescope)](https://it.wikipedia.org/wiki/Event_Horizon_Telescope) che ha portato alla fotografia del "secolo": la prima foto di un buco nero o meglio ombra del [buco nero M87](https://it.wikipedia.org/wiki/Buco_nero_supermassiccio). Dietrich ed il fratello Reinhold sono anche detentori dell'[SFSAward 2017](https://www.sfscon.it/awards/sfs-award-2017/) per i propri meriti nell'utilizzo e diffusione di Software Libero sul nostro territorio. 

{{% figure src="/img/black-hole-m87.jpg" caption="Buco Nero Supermassivo M87 (Fonte: Collaborazione EHT)" %}}

<!--more-->

**FUSS-Lab**: Sig. Pescoller, la contattiamo perchè lei qualche anno fa aveva tenuto un [talk alla conferenza internazionale SFSCon](https://www.sfscon.it/talks/alma-telescopes-powered-rtai-linux/) a proposito dell'uso di Free Software proprio nel progetto ALMA. Qual'è stata l'importanza del Free Software a suo avviso in questa affascinante notizia?

**D. Pescoller**: Confermo tutto ciò che avevo riportato a suo tempo nella presentazione e lo ribadisco di nuovo. Non vorrei dilungarmi troppo sui numerosi dettagli tecnologici per i quali chi è interessato può rivedere [il talk direttamente online](https://www.sfscon.it/talks/alma-telescopes-powered-rtai); oggi mi sentirei di dire che senza il Free Software questa foto non sarebbe stato possibile farla. Provate semplicemente a pensare cosa sarebbe internet senza il Free Software. Pensate all'importanza dell'uso delle e-mail in un simile progetto, la neccessità di coordinare i numerosi ricercatori dislocati in diverse parti del mondo. Pensate ai tanti forum dove vengono condivise e risolte problematiche in modo aperto ed accessibile a tutti: ogni piccolo avanzamento può essere condiviso. 

**FUSS-Lab**: La foto è stata fatta solo con Free Software?

**D. Pescoller**: Direi di no, sicuramente è stato usato anche tanto software proprietario, ma ovviamente per scopi secondari. L'uso del *closed source* nelle parti più delicate dell'immagine, renderebbe non credibile il risultato stesso. L'EHT in un certo senso ha applicato il concetteo del Free Software alla struttura hardware dei sistemi. L'EHT non è un solo telescopio ma una rete [VLBI (Very-long-baseline interferometry)](https://it.wikipedia.org/wiki/Very-long-baseline_interferometry) di tanti siti di osservazione sparsi in tutto il pianeta.

{{% figure src="/img/rete-vlbi.jpg" caption="Rete di osservatori in VLBI (Fonte: ESO/O. Furtak)" %}}

**FUSS-Lab** Lei conosce il progetto [FUSS](https://fuss.bz.it) e cosa ne pensa?

**D. Pescoller**: Certo, come no! In realtà, prima che entrassi in Microgate avevo avviato, assieme a mio fratello ed ai miei colleghi della QBUS, un piccolo progetto stile FUSS per tutte le scuole elementari e medie della Val Badia e Val Gardena. E` andato avanti per tanti anni però poi, a causa di decisioni prese dall'alto, è andato perso perché non sono più state distribuite in modo adeguato le risorse umane e gli investimenti.
Penso che il progetto FUSS a livello provinciale rappresenti qualcosa di unico come cultura digitale e che vada sostenuto, portato avanti ed anche ampliato. Il mio augurio è quello che ritorni presto FUSS anche in Val Badia ed in Val Gardena.

**FUSS-Lab**: Quale messaggio vuole lasciare ai nostri lettori?

**D. Pescoller**: La foto dell'M87 riassume e spiega quale possa essere la portata della mente umana: uno scienziato, 100 anni fa, con la sua mente straordinaria aveva già previsto tutto, per lui era tutto chiaro, ma l'umanità intera ci avrebbe impiegato ben 100 anni per verificare le sue teorie. In questo senso nelle scuole il nostro compito è quello di stimolare le menti dei giovani, non appiattirle e standardizzarle. La domanda che dobbiamo porci pertanto è la seguente: vogliamo crescere dei piccoli potenziali Einstein o dei piccoli robottini super produttivi?

{{% figure src="/img/alma-telescopes.jpg" caption="Il radiointerferometro ALMA (Atacama Large Millimeter/submillimeter Array) situato a 5000 metri d'altitudine nel deserto di Atacama in Cile." %}}
