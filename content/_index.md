FUSS è una soluzione GNU/Linux completa (server, client e desktop/standalone) basata su [Debian](https://www.debian.org) per la gestione di una rete didattica.

E' nel contempo anche un progetto di [sostenibilità digitale](https://openbz.eu/?lang=it) che dal 2005 permette ad alunni e docenti di usare a casa gli stessi strumenti informatici installati a scuola, liberamente e senza alcun aggravio di costo.
